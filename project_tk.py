#! /usr/bin/python3

import time
import requests
import json
from datetime import datetime as dt
from tkinter import *
from math import *

## this is bad
email = "you@genesys.com"
token = "yourAPItoken"

f = ('Times', 14)

base_get_url="https://cloud.skytap.com/v2/projects?count=900&scope=company&sort=created_at&query=name%3A-sandbox*%20-templates*%20-self*%20%2Cuser_id%3A174920"
base_delete_url="https://cloud.skytap.com/projects/"

main_window = Tk()
main_window.title('Project Search')
main_window.geometry('1620x900')
# main_window.config(bg='#0B5A81')

cb_value = []
cb_array = []

cb_index = IntVar() 
days = IntVar() 
envs = IntVar() 

prj_lbl = StringVar()

## functions to do stuff
def ButtonChecked(idx):
    # print ("index {0} value {1}".format(idx, cb_value[idx].get()))
    if cb_value[idx].get() == "116804":
        cb_array[idx].deselect()
    return

def Select_All():
    # print ("selecting all ", len(cb_array))
    for idx in range(len(cb_array)):
        cb_array[idx].select()
    if cb_value[0].get() == "116804":
        cb_array[0].deselect()

def Deselect_All():
    # print ("deselecting all ", len(cb_array))
    for idx in range(len(cb_array)):
        cb_array[idx].deselect()

def Delete_Selected():
    # print ("deleting selected")
    for idx in range(len(cb_array)):
        if cb_value[idx].get() != "":
            # print ("deleting ", cb_value[idx].get())
            delete_url = base_delete_url + cb_value[idx].get()
            headers = { 'Accept': 'application/json', 'Content-type': 'application/json'}
            auth = (email, token)   # login and password/API Token
            api_response = requests.delete(delete_url, headers=headers, auth=auth)
            # print ("delete response ", api_response)

    # refresh the projects list
    Send_Query()

def Send_Query():
    check_counter=0
    warn = ""

    email = email_entry.get()
    if email == "":
       warn = warn + "Email cannot be empty\n"
    else:
        check_counter += 1
        # print ("email ", email)

    token = token_entry.get()
    if token == "":
        warn = warn + "Token cannot be empty\n"
    else:
        check_counter += 1
        # print ("token ", token)

    days = day_entry.get()
    if days == "":
        warn = warn + "Days cannot be empty\n"
    else:
        if days.isnumeric():
            check_counter += 1
        else:
            warn = warn + "Days must be an integer\n"

    envs = env_entry.get()
    if envs == "":
        warn = warn + "Max Envs cannot be empty\n"
    else:
        if envs.isnumeric():
            check_counter += 1
        else:
            warn = warn + "Max Envs must be an integer\n"

    if check_counter == 4:
        canvas.delete('all')

        ## mangle the query url
        query_url = base_get_url + "&created_at=to%20now-" + str(days) + "d"

        ## auth
        headers = { 'Accept': 'application/json', 'Content-type': 'application/json'}
        auth = (email, token)   # login and password/API Token

        ## GET the results
        api_response = requests.get(query_url, headers=headers, auth=auth)
        # print ("get response ", api_response)
        # print (json.dumps(api_response.json(), indent = 3))

        ## do some stuff
        y = 0
        cb_index = 0
        cb_value.clear()
        cb_array.clear()
        for P in api_response.json():
            if P["configuration_count"] <= int(envs):
                ## place a checkbutton
                cb_value.append(StringVar())
                cb_value[-1].set("")
                cb_array.append(
                        Checkbutton(
                        canvas,
                        variable=cb_value[-1],
                        command=lambda cb_index=cb_index: ButtonChecked(cb_index),
                        onvalue=P["id"],
                        offvalue="",
                        cursor='hand2'
                        ))
                canvas.create_window(
                        10,
                        y+10,
                        window=cb_array[cb_index],
                        anchor=NW
                        )

                ## how many days old is it?
                today = dt.today()
                today = today.replace(hour=0, minute=0, second=0, microsecond=0)
                project_date = dt.strptime(P["created_at"].split()[0], '%Y/%m/%d')
                age = today - project_date
                # print ("Age " + str(age.days) + " P_created " + P["created_at"].split()[0] + " project date ", project_date)

                ## place a label with the project information
                label = Label(
                        canvas,
                        text=" " + P["id"] + " - " + P["name"] + "\n" +
                        " Environments " + str(P["configuration_count"]) + " -- Date Created " + P["created_at"].split()[0] + " -- Days Old " + str(age.days) + "\n" +
                        " Contact/Owner " + P["users"][1]["first_name"] + " " + P["users"][1]["last_name"] + " " + P["users"][1]["email"],
                        justify=LEFT)
                canvas.create_window(40, y, window=label, anchor=NW)

                ## move y to the next scroll position
                # y += 48
                y += 72
                cb_index += 1

        scrollbar = Scrollbar(canvas, orient=VERTICAL, command=canvas.yview)
        scrollbar.place(relx=1, rely=0, relheight=1, anchor=NE)
        canvas.config(yscrollcommand=scrollbar.set, scrollregion=(0, 0, 0, y))
        prj_lbl.set("Projects Found " + str(len(cb_array)))
    else:
        messagebox.showerror('', warn)

## widgets
left_frame = Frame(
    main_window, 
    bd=2, 
    bg='#CCCCCC',   
    relief=SOLID, 
    padx=10, 
    pady=10
    )

Label(
    left_frame, 
    text="Email", 
    bg='#CCCCCC',
    font=f,
    justify=LEFT
    ).grid(row=0, column=0, sticky=W, pady=10)

Label(
    left_frame, 
    text="API Token", 
    bg='#CCCCCC',
    font=f,
    justify=LEFT
    ).grid(row=1, column=0, sticky=W, pady=10)

Label(
    left_frame, 
    text="Days", 
    bg='#CCCCCC',
    font=f, 
    justify=LEFT
    ).grid(row=2, column=0, sticky=W, pady=10)

Label(
    left_frame, 
    text="Max Envs", 
    bg='#CCCCCC',
    font=f, 
    justify=LEFT
    ).grid(row=3, column=0, sticky=W, pady=10)

email_entry = Entry(
    left_frame, 
    width=24, 
    font=f
    )

token_entry = Entry(
    left_frame, 
    width=24, 
    font=f,
    show='*'
    )

day_entry = Entry(
    left_frame, 
    width=24, 
    font=f
    )

env_entry = Entry(
    left_frame, 
    width=24, 
    font=f
    )

select_all_btn = Button(
    left_frame, 
    width=10, 
    text='Select All', 
    font=f, 
    relief=SOLID,
    cursor='hand2',
    command=Select_All
    )

send_query_btn = Button(
    left_frame, 
    width=10, 
    text='Send Query', 
    font=f, 
    relief=SOLID,
    cursor='hand2',
    command=Send_Query
    )

deselect_all_btn = Button(
    left_frame, 
    width=10, 
    text='Deselect All', 
    font=f, 
    relief=SOLID,
    cursor='hand2',
    command=Deselect_All
    )

delete_selected_btn = Button(
    left_frame, 
    width=10, 
    text='Delete Selected', 
    font=f, 
    relief=SOLID,
    cursor='hand2',
    command=Delete_Selected
    )

right_frame = Frame(
    main_window, 
    bd=2, 
    bg='#CCCCCC',
    relief=SOLID, 
    padx=10, 
    pady=10
    )

Label(
    right_frame, 
    #text="Projects Found", 
    textvariable=prj_lbl, 
    bg='#CCCCCC',
    font=f,
    justify=CENTER
    ).grid(row=0, column=0, sticky=W, pady=10)

canvas = Canvas(right_frame,
        # bg="White",
        bg='#DDDDDD',
        width=1024,
        height=768
        )

## widget placement
email_entry.grid(row=0, column=1, pady=10, padx=20)
token_entry.grid(row=1, column=1, pady=10, padx=20)
day_entry.grid(row=2, column=1, pady=10, padx=20)
env_entry.grid(row=3, column=1, pady=10, padx=20)
select_all_btn.grid(row=4, column=0, pady=10, padx=10)
send_query_btn.grid(row=4, column=1, pady=10, padx=10)
deselect_all_btn.grid(row=5, column=0, pady=10, padx=10)
delete_selected_btn.grid(row=5, column=1, pady=10, padx=10)
left_frame.place(x=20, y=20)

right_frame.place(x=550, y=20)
canvas.grid(row=1, column=0)

## initialize some fields
email_entry.insert(0, email)
token_entry.insert(0, token)
day_entry.insert(0, 85)
env_entry.insert(0, 0)

## make it happen
main_window.mainloop()